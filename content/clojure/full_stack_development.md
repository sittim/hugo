---
title: Full Stack Development in Clojure
date: 2020-09-30T07:18:40-04:00
draft: false
---

# Clojure Full Stack Development

I am a minimalist, I prefer the fewest layers of abstractions that is
reasonable.

## Clojure Installation

Start here: [Getting Started](https://clojure.org/guides/getting_started)

## Dependency Management

The main options are *Leiningen*, *boot* and *deps.edn*.  The *deps.edn* is the
solution that gets the job done in a cleanest and lightest way.

## deps.edn

Example *deps.edn*

Awesome

```clojure {linenos=table,linenostart=1}
{:paths ["src" "resources"]
 :deps {org.clojure/clojure {:mvn/version "1.10.1"}}

 :aliases
 {:test {:extra-paths ["test"]}}}
```

### Line 1 => path to the resources

```text
├── my_app
│   ├── src
│   ├── resource
│   └── test
```

### Line 2 => List of the dependencies

### Line 4 => Beginning of the aliases

### Line 5 => `test` alies

