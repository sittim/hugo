---
title: "Hugo"
date: 2020-10-10T14:04:21-04:00
draft: false
---

# Hugo

Hugo make it easy and fast to build static websites.  The *Hugo* documentation
seems to be built by engineers for engineers.  The purpose of this site is to
provide easy to read tutorials to get started.  Throughout the site, links to
official Hugo documentation will be provided.


